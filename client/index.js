
import React from 'react';
import {AppRegistry, ImageBackground} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler'
import { Provider } from 'react-redux';
import configureStore from './src/store/configureStore';

const store = configureStore();

const todo = () => (
    <Provider store={store}>
         <App />
    </Provider>
);

AppRegistry.registerComponent(appName, () => todo );


