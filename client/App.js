
import React, { Component } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './src/screens/Login/Login'
import Todos from './src/screens/todos/todosContainer'
import TodoContent from './src/screens/todos/todoContent'
import todosNavigator from './src/screens/todos/todosNavigator'
import AddTodo from './src/screens/todos/addTask'

import {Text, Button} from 'react-native'

const Stack = createStackNavigator();

export default class App extends Component {
  
  render() {
    return(
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={({navigation}) =>  ({
              headerTitle: () => <Text>Welcome</Text>,
              headerRight: () => (
                <Button
                  onPress={() => navigation.navigate('Login')}
                  title="Exit"
                  color="orange"
                />
              ),
            })
              
            }
          />

          <Stack.Screen
             name="Todos" 
             component={todosNavigator}
             options={({navigation}) =>  ({
              headerTitle: () => <Text>Todos</Text>,
              headerRight: () => (
                <Button
                  onPress={() => navigation.navigate('addTask')}
                  title="Add Task"
                  color="blue"
                />
              ),
            })
              
            }
             />
      
          <Stack.Screen
            name="todoContent" 
            component={TodoContent}
            options={{
            headerTitle: () => <Text>Todo Content</Text>
          }}
          />

          <Stack.Screen
            name="addTask" 
            component={AddTodo}
            options={{
            headerTitle: () => <Text>Add todo</Text>
          }}
          />
          
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
};


