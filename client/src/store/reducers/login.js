import {LOGIN} from "../actions/actionTypes";
  
  const initialState = {
    user: undefined
  };
  
  export const loginReducer = (state = initialState, action) => {
    switch (action.type) {
      case LOGIN:
        return {
          ...state,
          user: action.user
        };
      
      default:
        return state;
    }
  };
  