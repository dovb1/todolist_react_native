import {ADD_TODO, DELETE_TODO, GET_TODOS, UPDATE_TODO, COMPLETE_TODOS} from "../actions/actionTypes";
  
  const initialState = {
    todos: null,
    completedTodos: null
  };
  
  export const todosReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_TODO:
        return {
          ...state,
          todos: [
            ...state.todos,
            action.newTodo
          ]
        };
    
      case GET_TODOS:
        return {
          ...state,
          todos: action.todos
        };
      
      case UPDATE_TODO:
        return {
          ...state,
          todos: state.todos.map((todo,i) => {
            if(todo._id === action.updatedTodo._id) {
              return Object.assign({}, todo, {
                imageUri: action.updatedTodo.imageUri,
                completed: action.updatedTodo.completed
              })
            } else {
              return todo
            }
          })
        } 

      case COMPLETE_TODOS:
        return {
          ...state,
          todos: action.completedTodos
        }
        
      case DELETE_TODO:
        return {
          ...state,
          todos: state.todos.filter(todo => {
            return todo._id !== action.todoID;
          })
        };
      
      default:
        return state;
    }
  };
  