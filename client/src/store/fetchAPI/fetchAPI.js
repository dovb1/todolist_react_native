export const fetchAPI = (url, userid, method, queryParams = null, data = null, todoid=null) => {
  switch(method) {
    case 'GET':
      return fetch('http://192.168.43.48:3000/' + url + "/" + userid + "/?" + queryParams,  {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then((response) => response.json())
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error(error);
      });
    
    case 'POST':
      return fetch('http://192.168.43.48:3000/' + url + "/" + userid + "/?" + queryParams,  {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({data:data})
      })
      .then((response) => response.json())
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error(error);
      });

    case 'DELETE':
    case 'PATCH':
      return fetch('http://192.168.43.48:3000/' + url + "/" + todoid + "/" + userid + "/?" + queryParams,  {
        method: method,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({data:data})
      })
      .then((response) => response.json())
      .then((res) => {
        return res;
      })
      .catch((error) => {
        console.error(error);
      });
  }




  // return fetch('http://192.168.43.48:3000/' + url + "/" + userid, method === 'GET' ? {
  //     method: method,
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json',
  //     }
  //   }: {
  //     method: method,
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({data:data})
  //   }).then((response) => response.json())
  //     .then((res) => {
  //       return res;
  //     })
  //     .catch((error) => {
  //       console.error(error);
  //     });
}