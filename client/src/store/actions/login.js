import {LOGIN} from "./actionTypes";
import {fetchAPI} from '../fetchAPI/fetchAPI'  
  
  export const login = (username, password) => {
    return (dispatch) => {
      return fetchAPI('login', null, "POST", null, {username, password})
      .then((data) => {
          dispatch({
              type: LOGIN,
              user: data.user
          })
      })
    }
    
  }