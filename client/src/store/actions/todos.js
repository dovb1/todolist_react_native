import {ADD_TODO, DELETE_TODO, GET_TODOS, UPDATE_TODO, COMPLETE_TODOS} from "./actionTypes";
import {fetchAPI} from '../fetchAPI/fetchAPI'  
  

  export const addTodo = (title, description, userid) => {
    return (dispatch) => {
      return fetchAPI('tasks', userid, "POST", null, {title, description})
      .then((data) => {
        dispatch({
          type: ADD_TODO,
          newTodo: data
        })
      })
    }
    
  }

  export const getTodos = (userid) => {
    return (dispatch) => {
      return fetchAPI('tasks', userid, "GET",  "completed=false")
      .then((data) => {
          dispatch({
              type: GET_TODOS,
              todos: data
          })
      })
    }
    
  }

  export const deleteTodo = (todoID, userid) => {
    return (dispatch) => {
      return fetchAPI('tasks', userid, "DELETE", null, null, todoID)
      .then(() => {
        dispatch({
          type: DELETE_TODO,
          todoID: todoID
        })
      })
    }
    
  }

  export const getCompletedTodos = (userid) => {
    return (dispatch) => {
      return fetchAPI('tasks', userid, 'GET', "completed=true")
      .then((data) => {
        dispatch({
          type: COMPLETE_TODOS,
          completedTodos: data
        })
      })
    }
    
  }

  export const updateTodo = (todoID, data, userid) => {
    return (dispatch) => {
      return fetchAPI('tasks', userid,  "PATCH", null,  {data}, todoID )
      .then((todo) => {
        dispatch({
          type: UPDATE_TODO,
          updatedTodo: todo
        })
      })
    }
    
  }
  