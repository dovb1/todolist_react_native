import React, { Component } from "react";
import {FlatList} from "react-native";
import {Spinner} from 'native-base';
import Todo from './todo'
import {getCompletedTodos, deleteTodo} from './../../store/actions/todos'
import { connect } from "react-redux";

 class CompleteTodos extends Component {
  constructor(props) {
    super(props) 
    this.state = {
      isLoading: true
    }
  }
    
  componentDidMount = () => {
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.onFocusFunction()
    })
  }

  onFocusFunction = () => {
    this.setState({
      isLoading: true
    }, () => {
      this.props.getCompletedTodosFromStore(this.props.user._id)
    })
    
  }

  // componentWillUnmount = () => {
  //   // this.setState({
  //   //   isLoading: true
  //   // })
  //   this.focusListener.remove()
  // }

  componentWillReceiveProps = (nextProps) => {
    this.setState({
      isLoading: nextProps.todos !== undefined && false
    })
  }

  navigate = (todoID) => {
    this.props.navigation.navigate('todoContent', {todoID:todoID})
  }

  deleteTodo = (todoID) => {
    this.props.deleteTodoFromStore(todoID, this.props.user._id)
  }

  render() {
    if(this.state.isLoading) {
      return (
        <Spinner color='red' />
      )
    } else {
      return ( 
        <FlatList
        data={this.props.todos}
        renderItem={({ item }) => <Todo todo={item} navigate = {this.navigate} deleteTodo = {this.deleteTodo}/>}
        keyExtractor={item => item.id}
      />  
  )
    }
    
  }  
  
}

const mapStateToProps = state => {
  return {
    user: state.loginReducer.user,
    todos: state.todosReducer.todos
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCompletedTodosFromStore: (userID) => dispatch(getCompletedTodos(userID)),
    deleteTodoFromStore: (todoID,userid) => dispatch(deleteTodo(todoID, userid))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CompleteTodos);
