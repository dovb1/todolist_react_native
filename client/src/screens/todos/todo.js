import React, { Component } from 'react';
import {View, TouchableHighlight} from 'react-native'
import {Card, CardItem, Body, Text, Button} from 'native-base';
import { styles } from './todosStyle';
import Icon from "react-native-vector-icons/Ionicons";



export default class Todo extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (

      <View style = {styles.todo}>
        <TouchableHighlight onPress = {() => this.props.navigate(this.props.todo._id)} activeOpacity={0.6} underlayColor="#DDDDDD" >
          <Card>
            <CardItem >
              <Body style = {styles.todoItems} >
                <Text>
                {this.props.todo.title}
                </Text>
                <Button style = {styles.deleteBtn} rounded danger onPress = {() => this.props.deleteTodo(this.props.todo._id)}>
                <Icon
                  size={30}
                  name={Platform.OS === "android" ? "md-trash" : "ios-trash"}
                  color="black"
                />
                  <Text> Delete </Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
        </TouchableHighlight>
      </View>
    )
  }
}