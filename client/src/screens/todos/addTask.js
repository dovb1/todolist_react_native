import React, { Component } from 'react';
import { Form, Item, Input, Label, Button, View, Text } from 'native-base';
import { connect } from "react-redux";
import {addTodo} from './../../store/actions/todos'

class AddTodo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            taskTitle : '',
            taskDescription: ''
        }
    }

    handleTitle = (text) => {
        if(text !== '') {
            this.setState({
                taskTitle: text
            })
        }
    }

    handleDescription = (text) => {
        if(text !== '') {
            this.setState({
                taskDescription: text
            })
        }
    }

    handleSubmit = () => {
        this.props.AddTodoToStore(this.state.taskTitle, this.state.taskDescription, this.props.user._id)
    }

    render() {
        const {taskTitle, taskDescription} = this.state
        return (
            <View>
                 <Form>
                    <Item floatingLabel>
                    <Label>Title</Label>
                    <Input onChangeText = {this.handleTitle} value = {taskTitle}/>
                    </Item>
                    <Item floatingLabel last>
                    <Label>Task</Label>
                    <Input onChangeText = {this.handleDescription} value = {taskDescription}/>
                    </Item>
                </Form>
                <Button onPress = {this.handleSubmit}>
                    <Text>Add Todo</Text>
                </Button>
            </View>
           
        )
    }
}

const mapStateToProps = state => {
    return {
      user: state.loginReducer.user
    };
  };

  const mapDispatchToProps = dispatch => {
    return {
      AddTodoToStore: (title, taskDescription, userid) => dispatch(addTodo(title, taskDescription, userid))
    };
  };
  
  export default connect(mapStateToProps, mapDispatchToProps)(AddTodo);