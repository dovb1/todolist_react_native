import React, { Component, useState } from "react";
import { Image, Text } from "react-native";
import Modal from 'react-native-modal';
import todoIcon from '../../Images/todoIcon.png'
import ImagePicker from 'react-native-image-picker';
import {updateTodo} from './../../store/actions/todos'
import { connect } from "react-redux";
import { Card, CardItem, Thumbnail, Button, Icon, Spinner, Left, Body, Right } from 'native-base';
import { styles } from "./todosStyle";

const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class TodoContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todoID: props.route.params.todoID,
      todo: undefined
    }
  }

  componentDidMount = () => {
    this.setState({
      todo: this.props.todos.find((todo, i) => todo._id === this.state.todoID)
    })
  }

  componentWillReceiveProps = (nextProps) => {
    let newTodo = nextProps.todos.find((todo, i) => todo._id === this.state.todoID)
    let preTodo = this.props.todos.find((todo, i) => todo._id === this.state.todoID)
    // let preTodo = {...this.state.todo}

    if(newTodo !== undefined && preTodo !== undefined) {
      this.setState({
        todo: newTodo
      }, () => {
        if(newTodo.completed !== preTodo.completed) {
          alert("todo updated")
        //   <Modal isVisible={this.state.isModalVisible}>
        //   <View style={{flex: 1}}>
        //     <Text>Todo Updated</Text>
        //     <Button title="Hide modal" onPress={this.toggleModal} />
        //   </View>
        // </Modal>
        }
      })
    }
   
    

    
  }
    
    pickImageHandler = () => {
      ImagePicker.launchCamera(options, (response) => {
        console.log('Response = ', response);
       
        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          console.log('User tapped custom button: ', response.customButton);
        } else {
          //const source = { uri: response.uri };
      
          // You can also display the image using data:
          const source = { uri: 'data:image/jpeg;base64,' + response.data };
         
          this.props.updateTodoToStore(this.state.todo._id, {imageUri: source.uri}, this.props.user._id)
        }
      });
    }

    handleDone = () => {
      this.props.updateTodoToStore(this.state.todo._id, {completed: true}, this.props.user._id)
    }

  render() {
    const { todo } = this.state;
   
    if(todo === undefined) {
      return (
        <Spinner color='red' />
      )
    } else {
      return (
        <Card>
                    <CardItem>
                    <Left>
                        <Thumbnail source={todoIcon} />
                        <Body>
                        <Text>{todo.description}</Text>
                        <Text note>GeekyAnts</Text>
                        </Body>
                    </Left>
                    </CardItem>
                    <CardItem cardBody>
                    <Image source={{uri: todo.imageUri}} style={{height: 200, width: 100, flex: 1}}/>
                    </CardItem>
                    <CardItem>
                    
                    <Left>
                   
                      <Button rounded light onPress = {this.pickImageHandler} disabled = {todo.completed}>
                          {/* <Icon name="md-trash" size={30} color="#900" /> */}
                          <Text>Add Picture</Text>
                      </Button>
                  
                    </Left>
                    <Body >
                      <Button style={{justifyContent:"center"}} rounded transparent success disabled = {todo.imageUri === null && !todo.completed}
                      onPress = {this.handleDone}>
                          {/* <Icon name="arrow-down" size={30} color="#900"/> */}
                          <Text>Done</Text>
                      </Button> 
                    </Body>
                    <Right>
                        <Text>11h ago</Text>
                    </Right>
                    </CardItem>
                </Card>
      )
    }
    
  }
  
}


const mapStateToProps = state => {
  return {
    user: state.loginReducer.user,
    todos: state.todosReducer.todos
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateTodoToStore: (todoID, data, userid) => dispatch(updateTodo(todoID, data, userid))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoContent);