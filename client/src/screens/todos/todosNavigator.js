import * as React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TodosScreen from './todosContainer'
import CompleteTodos from './compoletedTodos'

const Tab = createBottomTabNavigator();

export default function todosNavigator() {
  return ( 
      <Tab.Navigator initialRouteName="ToDo">
        <Tab.Screen name="ToDo" component={TodosScreen} />
        <Tab.Screen name="Completed" component={CompleteTodos} />
      </Tab.Navigator> 
  );
}