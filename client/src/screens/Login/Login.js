import React, { Component } from 'react'
import {TextInput, ImageBackground} from 'react-native'
import { connect } from "react-redux";
import { Button, Text as Text1 } from 'native-base';
import {View,Text} from 'react-native';
import {styles} from './LoginStyle'
import backgroundImage from "../../Images/todolistPattern.jpg";
import {login} from './../../store/actions/login'

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: null,
      password: null
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.user !== null) {
      this.props.navigation.navigate('Todos')
    }
  }

  handleUsername = (text) => {
    if(text !== '') {
      this.setState({
        username: text
      })
    }
  }

  handlePassword = (text) => {
    if(text !== '') {
      this.setState({
        password: text
      })
    }
  }

  handleLogin = () => {
    this.props.loginStore(this.state.username, this.state.password)
    // return fetch('http://192.168.43.48:3000/test', {
    //   method: 'GET',
    //   headers: {
    //     'Accept': 'application/json',
    //     'Content-Type': 'application/json',
    //   }
    // }).then((response) => response.json())
    //   .then((res) => {
    //    alert(res.data)
    //   })
    //   .catch((error) => {
    //     console.error(error);
    //   });
  }
  
  render() {
    const {username, password} = this.state
    return (
        <View style = {styles.container}>
          <View>
            <Text style = {styles.header}>Login</Text>
          </View>

          <View style = {styles.inputs}>
            <TextInput onChangeText = {this.handleUsername} style = {styles.input} placeholder="username"/>
            <TextInput onChangeText = {this.handlePassword} style = {styles.input} placeholder="password"/>
          </View>

          <View style = {styles.submitBtn}>
            <Button rounded primary onPress = {this.handleLogin} disabled = {username === null || password == null}>
              <Text1> Login </Text1>
            </Button>
          </View>
        </View>
    )
  }    
}




const mapStateToProps = state => {
  return {
    user: state.loginReducer.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginStore: (username, password) => dispatch(login(username, password))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

