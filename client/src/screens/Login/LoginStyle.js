import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
      flex: 1,
  },
  input: {
      height: 40,
      borderColor: 'grey',
      borderWidth: 1,
      margin: 10
  },
  header: {
    fontSize: 50,
    alignSelf: "center",
   
  },
  inputs: {
    marginTop: 50
  },
  submitBtn: {
      marginTop:30,
      alignSelf:"center"
  }
});