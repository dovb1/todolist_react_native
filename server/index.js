import express from 'express'
import './src/db/mongoose'
import userRouter from './src/routers/user'
import taskRouter from './src/routers/task'
import check from './src/middleware/check'
import bodyParser from 'body-parser'
import multer from 'multer'

const app = express()

//app.use(express.json())

app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))
app.use(check)
// app.use(multer({ dest: './uploads',
//   rename: function (fieldname, filename) {
//     return fieldname
//   },
// }))
app.use(userRouter)
app.use(taskRouter)
app.get('/test', function (req, res) {
    res.send({data: 'hello world'})
  })

const port = process.env.PORT || 3000

app.listen(port, () => {
    console.log('Server is up on port ' + port)
})