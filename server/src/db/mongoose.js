
import mongoose from 'mongoose'

mongoose.connect('mongodb://localhost:27017/Todo', {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
}).then(() => {
    console.log("connect to db")
}).catch(() => {
    console.log("dont connect")
})