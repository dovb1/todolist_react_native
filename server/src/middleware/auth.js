// const jwt = require('jsonwebtoken')
// const User = require('../models/user')
import User from '../models/user'

const auth = async (req, res, next) => {
    try {
        const user = await User.findOne({_id:req.params.userid})

        if (!user) {
            throw new Error()
        }
        
        req.user = user
        next()
    } catch (e) {
        console.log(e)
        res.status(401).send({ error: 'Please authenticate.' })
    }
}

module.exports = auth