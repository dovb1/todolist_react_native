
const check = async (req, res, next) => {
    try {
        console.log("check middleware")
        next()
    } catch (e) {
        res.status(401).send({ error: 'Please authenticate.' })
    }
}

module.exports = check