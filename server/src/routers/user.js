import  express from 'express'
import  User from '../models/user'
import auth from '../middleware/auth'

const router = new express.Router()

router.post('/login/:userid', async (req, res) => {
    
    //const user = new User(req.body)
    const username = req.body.data.username
    const password = req.body.data.password
    try {
        const user = await User.findOne({ username:username, password:password })
        if(!user) {
            user = new User(req.body.data.username, req.body.data.password)
            await user.save()
        }
        console.log('create new user or user entered')
        res.status(201).send({ user })
    } catch (e) {
        console.log('not create new user')
        res.status(400).send(e)
    }
})

// router.post('/users/login', async (req, res) => {
//     try {
//         const user = await User.findByCredentials(req.body.username, req.body.password)
        
//         res.send({ user })
//     } catch (e) {
//         res.status(400).send()
//     }
// })

module.exports = router