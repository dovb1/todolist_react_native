import  express from 'express'
import  Task from '../models/task'
import auth from '../middleware/auth'

const router = new express.Router()

router.post('/tasks/:userid', auth, async (req, res) => {
    const task = new Task({
        ...req.body.data,
        owner: req.user._id
    })

    try {
        await task.save()
        console.log("add new todo")
        res.status(201).send(task)
    } catch (e) {
        res.status(400).send(e)
    }
})

// GET /tasks?completed=true
// GET /tasks?limit=10&skip=20
// GET /tasks?sortBy=createdAt:desc
router.get('/tasks/:userid', auth, async (req, res) => {
    const match = {}
    

    if (req.query.completed) {
        match.completed = req.query.completed === 'true'
    }

    try {
        await req.user.populate({
            path: 'tasks',
            match
        }).execPopulate()
        res.send(req.user.tasks)
    } catch (e) {
        res.status(500).send()
    }
})

router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id

    try {
        const task = await Task.findOne({ _id, owner: req.user._id })

        if (!task) {
            return res.status(404).send()
        }

        res.send(task)
    } catch (e) {
        res.status(500).send()
    }
})

router.patch('/tasks/:todoid/:userid', auth, async (req, res) => {
    console.log('add picture')
    const updates = Object.keys(req.body.data.data)
    
    try {
        const task = await Task.findOne({ _id: req.params.todoid, owner: req.user._id})

        if (!task) {
            return res.status(404).send()
        }

        updates.forEach((update) => task[update] = req.body.data.data[update])
        await task.save()
        res.send(task)
    } catch (e) {
        res.status(400).send(e)
    }
})

router.delete('/tasks/:todoid/:userid', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({ _id: req.params.todoid, owner: req.user._id })

        if (!task) {
            res.status(404).send()
        }
        console.log("delete todo")
        res.send(true)
    } catch (e) {
        res.status(500).send()
    }
})

module.exports = router